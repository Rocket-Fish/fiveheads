<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>5Heads</title>

		<link rel="stylesheet" type="text/css" href="./css/menustyle.css">
		<style> body { margin: 0; } canvas { width: 100%; height: 100% } </style>
		
		<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/grid.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/fonts.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/demo.css" media="all">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="./js/script.js"></script>
		
		<script>
		</script>
	</head>
	
	<body>
		<?php
			require 'DB/DBLoginManager.php';
			require 'DB/DBDataManager.php';
			
		?>
		
	<div class="main-container" id="wide">
		<div id="top"></div>  	<!-- used to bring users to top -->
		<header id="main-header">
			<div class="content-inner">
				<div class="inner-container">
					<div class="row">
						<div class="column-large-12 center">
							<div class="row">
							
								<center><h2>5Heads</h2></center>

								<div id="menu">
									<ul>
										<li><a href='./'><span>Home</span></a></li>
										<li><a href='./viewMembers.php'><span>ViewMembers</span></a></li>
										<?php if(getAccountLevel($_SESSION['username']) >= 1) { ?>
											<li><a href='./viewWars.php'><span>ViewWars</span></a></li>
										<?php } ?>
										<?php if(getAccountLevel($_SESSION['username']) == 3) { ?>
											<li><a href='./addMembers.php'><span>AddMembers</span></a></li>
										<?php } ?>
										<li class='active'><a href='#'><span>AddWars</span></a></li>
										<li><a href='#'><span>About</span></a></li>
										<?php if($_SESSION['loggedin'] != true) { ?>
											<li class='last'><a href='./login.php'><span>Login</span></a></li>
										<?php } else { ?>										
											<li class='last'><a href='./userInformation.php'><span><?php echo $_SESSION['username'];?></span></a></li>
										<?php }?>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div><!-- end of class="inner-container" -->
			</div><!-- end of class="content-inner" -->
		</header><!-- end if id="main-header" -->
		<div id="Container">
			<div class="slide slide1" data-background="rgba(102, 102, 0,1.0)">
				<div class="main-container">
					<div class="container-wrap">
						<div class="playground">
							<?php if($_SESSION['loggedin'] != true || getAccountLevel($_SESSION['username']) <2) { ?>
								<h1> Restricted Access </h1>
								<h3> You do NOT have the rights to view this page </h3>
							<?php } else { ?>
								<h1>Record Wars</h1>
								<h3>Please input the war results using CSV format</h3>
								<form action="uploaded.php" method="post" enctype="multipart/form-data">
									Select text file to upload:
									<input type="file" name="fileToUpload" id="fileToUpload">
									<input type="submit" value="Upload file" name="submit">
								</form>	
							<?php
									} 
								?>

						</div>
					</div>
				</div>	
			</div>
			<div class="slide slide2" data-background="rgba(102, 102, 0,1.0)">
				<div class="main-container">
					<div class="container-wrap">
						<div class="playground">
						</div>
					</div>
				</div>
			</div>

		</div>
		</div>
	</body>
</html>