<?php 
	/////////////#4E41
	$host = 'localhost';
	$host_username = 'fivehea2_admin';
	$host_password = 'WSXZAQ!@#';
	$db_name = 'fivehea2_main';
	$tbl_name= 'UserAccountInformation';
	$war_tbl_name= 'WarInformation';

	function listMembersInTable() {
		global $host, $host_username, $host_password, $db_name, $tbl_name;
	
		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $tbl_name ORDER BY `averageScore` DESC";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
		$output = "<table><tr><td>Name</td><td>    </td><td>ThirtyAttackAveragePoint</td></tr>";
				
		while ($row = $result->fetch_array()) {
			$name = $row['username'];
			
			$score = $row['averageScore'];
			$output.= "<tr><td>".$name."</td><td>   </td><td>".$score."</td></tr>";
		}
		$conn->close();
		$output .= "</table>";
		
		return $output;
	}

	function listMembersInTable_Admin() {
		global $host, $host_username, $host_password, $db_name, $tbl_name;
	
		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $tbl_name ORDER BY `averageScore` DESC ";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
		$output = "<table><tr><td>Name</td><td>AdminLevel</td><td>ActivityLevel</td><td>    </td><td>ThirtyAttackAveragePoint</td></tr>";
				
		while ($row = $result->fetch_array()) {
			$name = $row['username'];
			$adminLevel = $row['level'];
			$activityLevel = $row['activelevel'];
			
			$score = $row['averageScore'];
			$output.= "<tr><td>".$name."</td><td>".$adminLevel."</td><td>".$activityLevel."</td><td>   </td><td>".$score."</td></tr>";
		}
		$conn->close();
		$output .= "</table>";
		
		return $output;
	}

	
	function listSpcificMember($username) {
		global $host, $host_username, $host_password, $db_name, $tbl_name;
	
		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $tbl_name WHERE username LIKE '$username'";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
		$output = "<table><tr><td>Name</td><td>AdminLevel</td><td>ActivityLevel</td></tr>";
		
		if($result->num_rows != 0) {
			$row = $result->fetch_assoc();
			$name = $row['username'];
			$adminLevel = $row['level'];
			$activityLevel = $row['activelevel'];
			$output.= "<tr><td>".$name."</td><td>".$adminLevel."</td><td>".$activityLevel."<td></tr></table>";
			$conn->close();
			return $output;
		} else {
			return "No Match Found";
		}
		
	}
	
	function getMemberDataTable($username) {
		global $host, $host_username, $host_password, $db_name;

		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $username LIMIT 0, 30 ";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
		$output = "<table border = \"1\"><tr><td>IDOfWarParticipated</td><td>StarsObtained</td><td>DamagePercent</td><td>Notes</td></tr>";
				
		while ($row = $result->fetch_array()) {
			$id = $row['warID'];
			$stars = $row['starsObtained'];
			$percent = $row['damagePercent'];
			$notes = $row['notes'];
			$output.= "<tr><td>".$id."</td><td>".$stars."</td><td>".$percent."<td><td>".$notes."<td></tr>";
		}
		$conn->close();
		$output .= "</table>";
		
		return $output;
	}
	
	function calculateMemberPointAverage($username) {
		global $host, $host_username, $host_password, $db_name;

		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $username LIMIT 0, 30 ";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
		$count = 0;
		$sum = 0;
		while ($row = $result->fetch_array()) {
			$stars = intval($row['starsObtained']);
			$percent = intval($row['damagePercent']);
			$th = intval($row['attackedTH']);

			$sum += ($stars*($percent/100)*$th);
			
			$count++;
		}
		$sum /= $count;
		$conn->close();
		
		return $sum;
	}
	
	function listWars() {
		global $host, $host_username, $host_password, $db_name, $war_tbl_name;
	
		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $war_tbl_name";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
		$output = "<table><tr><td>WarID</td><td>Date</td><td>War Type</td></tr>";
				
		while ($row = $result->fetch_array()) {
			$warID = $row['warID'];
			$warDate = $row['warDate'];
			$warType = $row['warType'];
			$output.= "<tr><td>".$warID."</td><td>".$warDate."</td><td>".$warType."<td></tr>";
		}
		$conn->close();
		$output .= "</table>";
		
		return $output;
	}
	
	function listSpcificWar($date) {
		global $host, $host_username, $host_password, $db_name, $war_tbl_name;
	
		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $war_tbl_name WHERE warDate LIKE '$date'";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
		$output = "<table><tr><td>WarID</td><td>War Date</td><td>War Type</td></tr>";
		
		if($result->num_rows != 0) {
			$row = $result->fetch_assoc();

			$warID = $row['warID'];
			$warDate = $row['warDate'];
			$warType = $row['warType'];
			$output.= "<tr><td>".$warID."</td><td>".$warDate."</td><td>".$warType."<td></tr>";

			$conn->close();
			return $output;
		} else {
			return "No Match Found";
		}
	}
	
	function getIDOfSpcificWar($date) {
		global $host, $host_username, $host_password, $db_name, $war_tbl_name;
	
		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $war_tbl_name WHERE warDate LIKE '$date'";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
		if($result->num_rows != 0) {
			$row = $result->fetch_assoc();

			$warID = $row['warID'];

			$conn->close();
			return $warID;
		} else {
			return false;
		}
	}
	
	function getIDOfLastWar() {
		global $host, $host_username, $host_password, $db_name, $war_tbl_name;
	
		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $war_tbl_name ORDER BY warId DESC LIMIT 1";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
		if($result->num_rows != 0) {
			$row = $result->fetch_assoc();

			$warID = $row['warID'];

			$conn->close();
			return $warID;
		} else {
			return false;
		}

	}
	
	function tableExists($conn, $table) {
		$res = $conn->query("SHOW TABLES LIKE '$table'");

		if(isset($res->num_rows)) {
			return true;
//			return $res->num_rows > 0 ? true : false;
		} else return false;
	}
	
	function saveWarData($fp) {
		global $host, $host_username, $host_password, $db_name, $war_tbl_name;

		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$successMessage = "";
		$linecount = 0;
		while ( ($line = fgets($fp)) !== false) {
			$linecount ++;
		}
		$successMessage .="Line Count:".$linecount."<br />";
		
		rewind($fp);
		$firstLine = fgetcsv($fp, 1000, ",");
		if($firstLine != null && count($firstLine) >= 2) {
			// check if name only contains letters and whitespace
			if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$firstLine[0])) {
				return "Error, Date format is incorrect, please use YYYY-MM-DD";
			} 
			
			if($firstLine[1] != "10" && $firstLine[1] != "15" && 
				$firstLine[1] != "20" && $firstLine[1] != "25" && 
				$firstLine[1] != "30" && $firstLine[1] != "35" && 
				$firstLine[1] != "40" && $firstLine[1] != "45" && 
				$firstLine[1] != "50") {
					return "Error, War type can only be 10, 15, 20, 25, 30, 35, 40, 45, or 50";
			}
			
		} else {
			return "Error reading file, unable to determine war type in line 1, make sure it is setup like the following <br /> date, typeOfWar";
		}
		
		$row = 1;
		while(($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
			$row++;
			
			if(!tableExists($conn, $data[0])) {
				return "Error, User ".$data[0]." does not exist. Please create this user";
			}
			
			if(count($data) != 5) {
				return "Error bad formatting on line ".$row." make sure the paremeters are setup like the following <br />attackerName,starsObtained,damagePercent,enemyTHLevel,notes";
			}
			
		}		

		if($linecount != (intval($firstLine[1])*2)+1) {
			return "Error make sure you have all the attack recorded ... you are supposed to have ".(intval($firstLine[1])*2)+1 ." attacks recorded <br />
			If someone didn't attack record it down as a 0 star and 0 percent damage";
		}

		///////----------- end of checking for errors
		$newID = getIDOfLastWar()+1;
		
		rewind($fp);
		$firstLine = fgetcsv($fp, 1000, ",");
		$intval =intval($firstLine[1]);
		$sql = "INSERT INTO $war_tbl_name (warID, warDate, warType)
			VALUES ('$newID', '$firstLine[0]', '$intval')";

		if ($conn->query($sql) === TRUE) {
			$successMessage .= "Date: ".$firstLine[0]." Type: ".$firstLine[1]."<br /> ";
		} else {
			$errorOut = ("Connection failed: " . $conn->error);
			$conn->close();
			return $errorOut."<br />Error!!!! unable to create war!!! chrck your first line";
		}	
			

		$row = 1;
		while(($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
			$row++;
			
			if(strcmp($data[0], "#4E41") === 0) {
				continue;
			}
			
			$sql = "INSERT INTO $data[0] (warID, attackedTH, starsObtained, damagePercent, notes)
				VALUES ('$newID', '$data[3]', '$data[1]', '$data[2]', '$data[4]')";
				
			if ($conn->query($sql) === TRUE) {
				
			} else {
				$errorOut = ("Connection failed: " . $conn->error);
				$conn->close();
				return $errorOut."<br />Error!!!! unable save data on line ".$row;
			}	
			
		}		
		
		return $successMessage;
	}
	
	function LinkingTables() {
		global $host, $host_username, $host_password, $db_name, $tbl_name;
	
		$conn = new mysqli("$host", "$host_username", "$host_password", "$db_name");
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 		
		
		$sql = "SELECT * FROM $tbl_name";
		$result = $conn->query($sql) or die(mysqli_error($conn));
		
				
		while ($row = $result->fetch_array()) {
			$name = $row['username'];
			
			$notAdminNum = 0;
			if($name != 'admin') {
				$notAdminNum = intval(calculateMemberPointAverage($name));
				
				$sql = "UPDATE $tbl_name SET averageScore='$notAdminNum' WHERE username LIKE '$name'";
				if ($conn->query($sql) === TRUE) {
					echo "Record updated successfully ".$name."-->".$notAdminNum."<br />";
				} else {
					echo "Error updating record: " . $conn->error."<br />";
				}
			}
		}
		$conn->close();
	}
?>
