<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>5Heads</title>

		<link rel="stylesheet" type="text/css" href="./css/menustyle.css">
		<link href="css/hover-min.css" rel="stylesheet" media="all">
		<style> body { margin: 0; } canvas { width: 100%; height: 100% } </style>
		
		<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/grid.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/fonts.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/demo.css" media="all">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="./js/script.js"></script>
		
		<script>
		</script>
	</head>
	
	<body>
	<?php
			require 'DB/DBLoginManager.php';
			
			// define variables and set to empty values
			$confirmPasswordErr = $newPasswordErr = $passwordErr = "";
			$confirmPassword = $newPassword = $password = "";
			$passwordUpdated = false;

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				
				if (empty($_POST["password"])) {
					$passwordErr = "Old Password Required";
				} else {
					$password = test_input($_POST["password"]);
					if (!preg_match("/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/",$password)) {
						$passwordErr = "Invalid Old Password";
					}
				}

				if (empty($_POST["newpassword"])) {
					$newPasswordErr = "New Password Required";
				} else {
					$newPassword = test_input($_POST["newpassword"]);
					if (!preg_match("/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/",$password)) {
						$newPasswordErr = "Invalid New Password";
					}
				}

				if (empty($_POST["confirmpassword"])) {
					$confirmPasswordErr = "Confirming Password Required";
				} else {
					$confirmPassword = test_input($_POST["confirmpassword"]);
					if (!preg_match("/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/",$password)) {
						$confirmPasswordErr = "Invalid Confirming Password";
					}
				}
				
				if( $passwordErr == "" && $newPasswordErr == "" && $confirmPasswordErr == "") {
					if($newPassword == $confirmPassword) {
						if(changePassword($_SESSION['username'], $password, $newPassword)) {
							$accountCreated = "Successfully Updated Password";
						} else {
							$accountCreated = "Invalid Original Password";
						}
					} else {
						$accountCreated = "New Password does not match Confirm Password";
					}
				}
			}
			function test_input($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data);
				return $data;
			}
	?>
		
	<div class="main-container" id="wide">
		<div id="top"></div>  	<!-- used to bring users to top -->
		<header id="main-header">
			<div class="content-inner">
				<div class="inner-container">
					<div class="row">
						<div class="column-large-12 center">
							<div class="row">
							
								<center><h2>5Heads</h2></center>

								<div id="menu">
									<ul>
										<li><a href='#'><span>Home</span></a></li>
										<li><a href='./viewMembers.php'><span>ViewMembers</span></a></li>
										<?php if(getAccountLevel($_SESSION['username']) >= 1) { ?>
											<li><a href='./viewWars.php'><span>ViewWars</span></a></li>
										<?php } ?>
										<?php if(getAccountLevel($_SESSION['username']) == 3) { ?>
											<li><a href='./addMembers.php'><span>AddMembers</span></a></li>
										<?php } ?>
										<?php if(getAccountLevel($_SESSION['username']) >=2) { ?>
											<li><a href='./addWars.php'><span>AddWars</span></a></li>
										<?php } ?>
										<li><a href='#'><span>About</span></a></li>
										<?php if($_SESSION['loggedin'] != true) { ?>
											<li class='last active'><a href='./login.php'><span>Login</span></a></li>
										<?php } else { ?>										
											<li class='last active'><a href='./userInformation.php'><span><?php echo $_SESSION['username'];?></span></a></li>
										<?php }?>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div><!-- end of class="inner-container" -->
			</div><!-- end of class="content-inner" -->
		</header><!-- end if id="main-header" -->
		<div id="Container">
			<div class="slide slide1" data-background="rgba(102, 102, 0,1.0)">
				<div class="main-container">
					<div class="container-wrap">
						<div class="playground">
							<?php if($_SESSION['loggedin'] != true || getAccountLevel($_SESSION['username']) <1) { ?>
								<h1> Restricted Access </h1>
								<h3> You MUST login to view this page </h3>
							<?php } else { ?>
							<h1>User Information</h1>
							<h4><?php echo $_SESSION['username']; ?> </h4>
							<a href="logout.php" style = "    display: inline-block;
							padding: 1em;
							background-color: #445561;
							border: 2px solid #445561;
							border-radius: 3px;
							text-transform: uppercase;
							text-decoration: none;
							color: white;
							" class="button hvr-sweep-to-right">Logout</a>
						</div>
					</div>
				</div>	
			</div>
			<div class="slide slide2" data-background="rgba(102, 102, 0,1.0)">
				<div class="main-container">
					<div class="container-wrap">
						<div class="playground">
							<h2>Change Password</h2>
								<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" style = "color: white;">
									Original Password: <input type="password" name="password" value="" maxlength = 30><br/>
									New Password: <input type="password" name="newpassword" value="" maxlength = 30><br/>
									Confirm Password: <input type="password" name="confirmpassword" value="" maxlength = 30><br/>
									<input type="submit" name="submit" value="Submit">
								</form>
								<?php echo "<p style = \"color:white;\">".$passwordErr."<p>";?>
								<?php echo "<p style = \"color:white;\">".$newPasswordErr."<p>";?>
								<?php echo "<p style = \"color:white;\">".$confirmPasswordErr."<p>";?>
								<?php echo "<p style = \"color:white;\">".$accountCreated."<p>";?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</body>
</html>
