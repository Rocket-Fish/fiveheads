<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>5Heads</title>

		<link rel="stylesheet" type="text/css" href="./css/menustyle.css">
		<style> body { margin: 0; } canvas { width: 100%; height: 100% } </style>
		
		<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/grid.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/fonts.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/demo.css" media="all">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="./js/script.js"></script>
		
		<script>
		</script>
	</head>
	
	<body>
		<?php
			require 'DB/DBLoginManager.php';
			
			// define variables and set to empty values
			$usernameErr = $passwordErr = "";
			$username = $password = "";
			$accountCreated = false;

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
			  
				if (empty($_POST["username"])) {
					 $nameErr = "User name is required";
			    } else {
					$username = test_input($_POST["username"]);
					// check if name only contains letters and whitespace
					if (!preg_match("/^[a-zA-Z ]*$/",$username)) {
						$usernameErr = "Only letters and white space allowed";
					}
				}
			  		    
				
				if (empty($_POST["password"])) {
					$passwordErr = "Password Required";
				} else {
					$password = test_input($_POST["password"]);
					// check if URL address syntax is valid (this regular expression also allows dashes in the URL)
					if (!preg_match("/^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/",$password)) {
						$passwordErr = "Invalid Password";
					}
				}
				
				if($usernameErr == "" && $passwordErr == "") {
					if(createAccount($username, $password)) {
						$accountCreated = true;
					} else {
						$accountCreated = "ERROR";
					}
				}
			}
			function test_input($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data);
				return $data;
			}
			
		?>
		
	<div class="main-container" id="wide">
		<div id="top"></div>  	<!-- used to bring users to top -->
		<header id="main-header">
			<div class="content-inner">
				<div class="inner-container">
					<div class="row">
						<div class="column-large-12 center">
							<div class="row">
							
								<center><h2>5Heads</h2></center>

								<div id="menu">
									<ul>
										<li><a href='./'><span>Home</span></a></li>
										<li><a href='./viewMembers.php'><span>ViewMembers</span></a></li>
										<?php if(getAccountLevel($_SESSION['username']) >= 1) { ?>
											<li><a href='./viewWars.php'><span>ViewWars</span></a></li>
										<?php } ?>
										<li class='active'><a href='#'><span>AddMembers</span></a></li>
										<?php if(getAccountLevel($_SESSION['username']) >=2) { ?>
											<li><a href='./addWars.php'><span>AddWars</span></a></li>
										<?php } ?>
										<li><a href='#'><span>About</span></a></li>
										<?php if($_SESSION['loggedin'] != true) { ?>
											<li class='last'><a href='./login.php'><span>Login</span></a></li>
										<?php } else { ?>										
											<li class='last'><a href='./userInformation.php'><span><?php echo $_SESSION['username'];?></span></a></li>
										<?php }?>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div><!-- end of class="inner-container" -->
			</div><!-- end of class="content-inner" -->
		</header><!-- end if id="main-header" -->
		<div id="Container">
			<div class="slide slide1" data-background="rgba(102, 102, 0,1.0)">
				<div class="main-container">
					<div class="container-wrap">
						<div class="playground">
							<?php if($_SESSION['loggedin'] != true || getAccountLevel($_SESSION['username']) <3) { ?>
								<h1> Restricted Access </h1>
								<h3> You do NOT have the rights to view this page </h3>
							<?php } else { ?>
								<h1>Add Members</h1>
									<?php 
										if($accountCreated == false) {
									?>
										<p>
											<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" style = "color: white;">
												UserName (NO SPACES): <input type="text" name="username" value="<?php echo $username;?>" maxlength = 30><br/>
												Password: <input type="password" name="password" value="" maxlength = 30><br/>
												<input type="submit" name="submit" value="Submit">
											</form>
											<?php echo "<p style = \"color:white;\">".$usernameErr."<p>";?> 
											<?php echo "<p style = \"color:white;\">".$passwordErr."<p>";?>
											<?php if($accountCreated == "ERROR"){echo "<p style = \"color:white;\"> Member Creation Error, Please use another User Name <p>";} ?>
										</p>
								<?php
										}else{
											echo "<h3 style = \"color:white;\"> Member Added </h3>";
										}
									
									} 
								?>

						</div>
					</div>
				</div>	
			</div>
			<div class="slide slide2" data-background="rgba(102, 102, 0,1.0)">
				<div class="main-container">
					<div class="container-wrap">
						<div class="playground">
						</div>
					</div>
				</div>
			</div>

		</div>
		</div>
	</body>
</html>