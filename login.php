<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>5Heads</title>

		<link rel="stylesheet" type="text/css" href="./css/menustyle.css">
		<style> body { margin: 0; } canvas { width: 100%; height: 100% } </style>
		
		<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/grid.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/fonts.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/demo.css" media="all">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="./js/script.js"></script>
		
		<script>
		</script>
	</head>
	
	<body>
		<?php
			require 'DB/DBLoginManager.php';

			// define variables and set to empty values
			$usernameErr = $loginError = $passwordErr = "";
			$username = $password = "";

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
			  
				if (empty($_POST["username"])) {
					$usernameErr = "username is required";
				} else {
					$username = test_input($_POST["username"]);
					// check if name only contains letters and whitespace
					if (!preg_match("/^[a-zA-Z ]*$/",$username)) {
						$usernameErr = "Only letters and white space allowed";
					}
				}
				
				if (empty($_POST["password"])) {
					$passwordErr = "Password Required";
				} else {
					$password = test_input($_POST["password"]);
				}
				
				if($usernameErr =="" && $passwordErr == "") {
					if(attemptLogin($username, $password)) {
						header("location: index.php");
					} else {
						$loginError = "Invalid username or Password";
					}
				}

			}
			function test_input($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data);
				return $data;
			}
			
		?>
		
	<div class="main-container" id="wide">
		<div id="top"></div>  	<!-- used to bring users to top -->
		<header id="main-header">
			<div class="content-inner">
				<div class="inner-container">
					<div class="row">
						<div class="column-large-12 center">
							<div class="row">
							
								<center><h2>5Heads</h2></center>

								<div id="menu">
									<ul>
										<li><a href='./'><span>Home</span></a></li>
										<li><a href='./viewMembers.php'><span>ViewMembers</span></a></li>
										<li><a href='#'><span>About</span></a></li>
										<li class='last active'><a href='#'><span>Login</span></a></li>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div><!-- end of class="inner-container" -->
			</div><!-- end of class="content-inner" -->
		</header><!-- end if id="main-header" -->
		<div id="Container">
			<div class="slide slide1" data-background="rgba(153, 153, 0,1.0)">
				<div class="main-container">
					<div class="container-wrap">
						<div class="playground">
							<h1>Login</h1>
								<p>
									<?php if ($_SESSION['loggedin'] == true)  { echo "<h3> You have already Logged In </h3>";} else { ?>
									<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" style = "color: white;">
										UserName: <input type="text" name="username" value="<?php echo $username;?>" maxlength = 50>
										Password: <input type="password" name="password" value="" maxlength = 30>
										<input type="submit" name="submit" value="Submit">
									</form>
									<?php  echo "<p style = \"color:white;\">".$usernameErr."<p>";?> 
									<?php echo "<p style = \"color:white;\">".$passwordErr."<p>";?>
									<?php echo "<p style = \"color:white;\">".$loginError."<p>";?>
									
									<p>Don't have an account? Ask your the members of the clan for an account</p>
									<?php } ?>
								</p>
						</div>
					</div>
				</div>	
			</div>
		</div>
		</div>
	</body>
</html>