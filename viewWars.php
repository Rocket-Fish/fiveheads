<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>5Heads</title>

		<link rel="stylesheet" type="text/css" href="./css/menustyle.css">
		<style> body { margin: 0; } canvas { width: 100%; height: 100% } </style>
		
		<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/grid.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/fonts.css" media="all">
		<link rel="stylesheet" type="text/css" href="./css/demo.css" media="all">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="./js/script.js"></script>
		
		<script>
		</script>
	</head>
	
	<body>
		<?php
			require 'DB/DBDataManager.php';
			require 'DB/DBLoginManager.php';
			
			$displaySpcific = $date = $dateErr = "";
			
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
			
				if($_POST['submit'] == "Clear") {
					$displaySpcific = "";
				} 

			
				if($_POST['submit'] == "Submit") {
				
					if (empty($_POST["date"])) {
						 $dateErr = "Date is Required to display spcific date";
					} else {
						$date = test_input($_POST["date"]);
						// check if name only contains letters and whitespace
						if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
							$dateErr = "Date format is YYYY-MM-DD";
						} 
					}
					if($dateErr == "") {
						if(listSpcificWar($date) != false) {
							$displaySpcific = listSpcificWar($date);
						}
					}
				
				}
			}			
			function test_input($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data);
				return $data;
			}
			
		?>
		
	<div class="main-container" id="wide">
		<div id="top"></div>  	<!-- used to bring users to top -->
		<header id="main-header">
			<div class="content-inner">
				<div class="inner-container">
					<div class="row">
						<div class="column-large-12 center">
							<div class="row">
							
								<center><h2>5Heads</h2></center>

								<div id="menu">
									<ul>
										<li><a href='./'><span>Home</span></a></li>
										<li><a href='./viewMembers.php'><span>ViewMembers</span></a></li>
										<li><a href='./viewWars.php'><span>ViewWars</span></a></li>
										<?php if(getAccountLevel($_SESSION['username']) == 3) { ?>
											<li><a href='./addMembers.php'><span>AddMembers</span></a></li>
										<?php } ?>
										<?php if(getAccountLevel($_SESSION['username']) >=2) { ?>
											<li><a href='./addWars.php'><span>AddWars</span></a></li>
										<?php } ?>
										<li><a href='#'><span>About</span></a></li>
										<?php if($_SESSION['loggedin'] != true) { ?>
											<li class='last'><a href='./login.php'><span>Login</span></a></li>
										<?php } else { ?>										
											<li class='last'><a href='./userInformation.php'><span><?php echo $_SESSION['username'];?></span></a></li>
										<?php }?>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div><!-- end of class="inner-container" -->
			</div><!-- end of class="content-inner" -->
		</header><!-- end if id="main-header" -->
		<div id="Container">
			<div class="slide slide1" data-background="rgba(102, 102, 0,1.0)">
				<div class="main-container">
					<div class="container-wrap">
						<div class="playground">
							<?php if($_SESSION['loggedin'] != true || getAccountLevel($_SESSION['username']) < 1) { ?>
								<h1> Restricted Access </h1>
								<h3> You do NOT have the rights to view this page </h3>
							<?php } else { ?>
								<h1>View Wars</h1>
								<h3>Search</h3>
								<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
									Date of War: <input type="text" name="date" value="<?php echo $date;?>" maxlength = 30><br/>
									<input type="submit" name="submit" value="Submit">
								</form>
								<?php echo "<p style = \"color:white;\">".$dateErr."<p>";?> 
						</div>
					</div>
				</div>	
			</div>
			<div class="slide slide5" data-background="rgba(200, 200, 0,1.0)">
				<div class="main-container">
					<div class="container-wrap">
						<div class="playground">
						<?php
								if($displaySpcific != "") {
									echo "<h2> Search Result </h2>";
								} else {
									echo "<h2> War List </h2>";
								}
						?>
						</div>
						<div style = "
						position: absolute;
						right: 40%;
						border: 3px solid #73AD21;
						text-align: center;
						">
						<?php
								if($displaySpcific != "") {
									echo $displaySpcific;
						?>
						
								<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
									<input type="submit" name="submit" value="Clear">
								</form>
						<?php
								} else {
									echo listWars(); 
								}
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</body>
</html>